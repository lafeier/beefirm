# Beefirm
![beefirm](./favicon.ico)

beefirm，毕方，源自山海经异兽中的一种神鸟。

beefirm定义了一份简单的类似java的语法，直接生成类图，相比传统的拖拽的画图工具提供了更高的灵活性，并且支持字段标注，方法标注，字段/参数和类之间的关联，让你更高效阅读源码，理解源码！

# 效果图

![image-20211209232916946](https://tva1.sinaimg.cn/large/008i3skNgy1gx8018b4m5j30sm0o2wh4.jpg)

beefirm定义一份类图，几乎和java语法没有任何区别，而且还简化了一些（类型，可见性等可省略不写）：

```beefirm
interface Interface01
interface Interface02
class Class01
class Class03
class Class04
class Class02 extends Class01 implements Interface01,Interface02{
    //字段f1
    public override void f1;
    //+right 字段f2
    public override void f2;
    public override Class03 f3;
    /*
    方法m1
    */
    public override void m1(
        /*参数p1*/
        int p1,
        /*参数p2*/
        string p2
    );
    public void m2(Class04 p1);
}
```
# 已实现功能

## 类定义
```beefirm
class Class01
class Class01{}
```

## 抽象类定义 
```beefirm
abstract class Class01
abstract class Class01{}
```

## 接口定义 
```beefirm
interface Interface01
interface Interface01{}
```

## 继承
```beefirm
class Class02 extends Class01
```

## 实现
```beefirm
class Class02 implements Interface01,Interface02
```

## 聚合

表示做为类**成员字段**，显示为**实心虚线菱形**

```
class Class02{
	Class03 f2;
}
class Class03
```

![image-20211209230941378](https://tva1.sinaimg.cn/large/008i3skNgy1gx7zgus0j7j308g0dc0t0.jpg)

<u>***note：成员字段的类型需要定义才会绘图***</u>



## 关联

作为**方法参数**or**返回值**,显示为**空心虚线菱形**的

```
//参数
class Class02 {
    public  void m1(Class03 p1);
}
class Class03

//返回值
class Class02 {
    public  Class03 m1();
}
class Class03
```

![image-20211209230753744](https://tva1.sinaimg.cn/large/008i3skNgy1gx7zez4ko1j30jg0cw0td.jpg)

## 范型

支持`字段类型`，`返回值类型`，`参数类型`的范型定义

支持`List<String>` `List[String]`两种方式定义范型

支持范型的聚合/关联关系，如`List<Person> `会和Person产生关系，而不是List

## 注释

  - 单行注释

    `//注释`

  - 多行注释

    `/*注释*/`

- 注释方向

默认：`//+left` or `/*+left 注释*/`

支持：`left`,`right`,`top`(方法除外),`bottom`(方法除外)

e g .

`//+right 注释`

`/*+right 注释*/`



![image-20211209215559296](https://tva1.sinaimg.cn/large/008i3skNgy1gx7xc4taruj30o208saat.jpg)

## 字段定义

- 可见性（可选）

- 类型 (可选)

- 字段名

- 注释 （可选） 

- 重载

    ```java
    //+right 字段age
    public override void age;
    ```

![image-20211209214454505](https://tva1.sinaimg.cn/large/008i3skNgy1gx7x0l01mlj30d207iq35.jpg)

## 方法定义

- 可见性（可选）
- 类型（可选）
- 方法名
- 方法注释 （可选）
- 参数列表（可选）
- 参数注释，**只能用多行注释（**可选）

```beefirm
/*
	方法m1
*/
public override void m1(
  /*参数p1*/
  int p1,
  /*参数p2*/
  string p2
);
```

![image-20211209221346138](https://tva1.sinaimg.cn/large/008i3skNgy1gx7xum8v7xj30mu0900to.jpg)

# 使用指南

## 编译安装

下载二进制包beefirm-${version}.tar.gz,解压

运行`./bin/beefirm xxx.beefirm`

:smile:  : 输出内容可使用plantuml官方的online server渲染,也可以自行下载安装plantuml

参考：https://plantuml.com/zh/

运行`./bin/beefirm xxx.beefirm -url` ，会生成一个预览连接，点击查看

### 全局可用配置

配置环境变量

```angular2html
export BEEFIRM_HOME=~/dev/env/beefirm
export PATH=$BEEFIRM_HOME/bin:$PATH
```

然后就可以使用了，直接: `beefirm xxx.beefirm`

## 开发构建（针对开发者）

在源码目录下./dev-build，就会将新构建的jar包放到BEEFIRM_HOME/lib下

然后就可以使用命令：beefirm xxx.beefirm 测试了
beefirm xxx.beefirm -url 可以生成一个在线预览的链接

# VSCode插件支持

项目地址：https://gitee.com/lafeier/beefirm-vscode

支持vscode直接编写预览，目前已经支持语法高亮，使用步骤：

1.安装插件，插件在：plugins/beefirm-0.0.1.vsix

2.编辑代码

3.右键，beefirm

![](https://tva1.sinaimg.cn/large/008i3skNgy1gyi1d49emdj30uk0hq0uz.jpg)

![](https://tva1.sinaimg.cn/large/008i3skNgy1gyiu1e5cggj310e0gsgn9.jpg)

# todo

- [x] 聚合/关联关系中对范化类型的支持，如 `List<String>`
- [ ] 集成plantuml api，提供通用api，生产图片，svg，文件流用于与其他系统集成
- [ ] 排版支持(继承/实现/聚合/组合)，线框的位置
- [ ] 语法高亮（可借用java的）
- [ ] web服务开发（前台+后台），实现在线编辑，预览
- [ ] 支持更多图，比如流程图
- [ ] 枚举
