grammar Beefirm;

javaCode
    :classDef* EOF
    ;
classDef
    : comments+=commentDef* KW_ABSTRACT? kind=(KW_CLASS|KW_INTERFACE) name=IDENTIFIER extendDef? implementDef? classBody?
    ;
extendDef
    : 'extends' superClass=IDENTIFIER
    ;
implementDef
    : 'implements' superInterfaces+=IDENTIFIER ( ',' superInterfaces+=IDENTIFIER)*
    ;
classBody
    : '{'
          (fields+=fieldDef|methods+=methodDef)*
       '}'
    ;
fieldDef
    : horizontal_line? comments+=commentDef* VISIBILITY? KW_OVERRIDE? type=typeDef? fieldName=IDENTIFIER KW_SEMICOLON
    ;
typeDef
    : IDENTIFIER
    | IDENTIFIER ('<'|'[') typeDef (',' typeDef)*  ('>'|']')
    ;

commentDef
    :SIMPLE_COMMENT
    |BRACKETED_COMMENT
    ;
methodDef
    : horizontal_line? comments+=commentDef* VISIBILITY? KW_OVERRIDE? rtnType=typeDef? methodName=IDENTIFIER '(' paramSeq ')' KW_SEMICOLON
    ;
paramSeq
    : params+=paramDef? (',' params+=paramDef)*
    ;
paramDef
    : comments+=BRACKETED_COMMENT* type=typeDef? paramName=IDENTIFIER
    ;
horizontal_line
    : HORIZONTAL_LINE1
    |HORIZONTAL_LINE2
    |HORIZONTAL_LINE3
    |HORIZONTAL_LINE4
    ;

VISIBILITY
    : 'public'
    | 'private'
    ;
KW_CLASS
    : 'class'
    ;
KW_INTERFACE
    : 'interface'
    ;
KW_ABSTRACT
    : 'abstract'
    ;
KW_SEMICOLON
    : ';'
    ;
KW_OVERRIDE
    :'override'
    ;
IDENTIFIER
    : (DIGIT | LETTER | CN_CHAR | '_' )+
    ;

fragment DIGIT
    : [0-9]
    ;

fragment LETTER
    : [A-Za-z]
    ;

fragment CN_CHAR
    : [\u4E00-\u9FA5]
    ;

SIMPLE_COMMENT
    : '//' ~[\r\n]+ '\n'?
    ;


HORIZONTAL_LINE1
    : '/*..*/'
    | '/*..' .*? '..*/'
    ;
HORIZONTAL_LINE2
    : '/*__*/'
    | '/*__' .*? '__*/'
    ;
HORIZONTAL_LINE3
    : '/*--*/'
    | '/*--' .*? '--*/'
    ;
HORIZONTAL_LINE4
    : '/*==*/'
    | '/*==' .*? '==*/'
    ;
BRACKETED_COMMENT
    : '/*' .*? '*/'
    ;

WS
    : [ \r\n\t]+ ->channel(HIDDEN)
    ;