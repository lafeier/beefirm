// Generated from /Users/lafeier/dev/code/beefirm/src/main/java/Beefirm.g4 by ANTLR 4.9.2
package com.lafeier.beefirm.antlr4;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BeefirmParser}.
 */
public interface BeefirmListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link BeefirmParser#javaCode}.
	 * @param ctx the parse tree
	 */
	void enterJavaCode(BeefirmParser.JavaCodeContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeefirmParser#javaCode}.
	 * @param ctx the parse tree
	 */
	void exitJavaCode(BeefirmParser.JavaCodeContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeefirmParser#classDef}.
	 * @param ctx the parse tree
	 */
	void enterClassDef(BeefirmParser.ClassDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeefirmParser#classDef}.
	 * @param ctx the parse tree
	 */
	void exitClassDef(BeefirmParser.ClassDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeefirmParser#extendDef}.
	 * @param ctx the parse tree
	 */
	void enterExtendDef(BeefirmParser.ExtendDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeefirmParser#extendDef}.
	 * @param ctx the parse tree
	 */
	void exitExtendDef(BeefirmParser.ExtendDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeefirmParser#implementDef}.
	 * @param ctx the parse tree
	 */
	void enterImplementDef(BeefirmParser.ImplementDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeefirmParser#implementDef}.
	 * @param ctx the parse tree
	 */
	void exitImplementDef(BeefirmParser.ImplementDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeefirmParser#classBody}.
	 * @param ctx the parse tree
	 */
	void enterClassBody(BeefirmParser.ClassBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeefirmParser#classBody}.
	 * @param ctx the parse tree
	 */
	void exitClassBody(BeefirmParser.ClassBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeefirmParser#fieldDef}.
	 * @param ctx the parse tree
	 */
	void enterFieldDef(BeefirmParser.FieldDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeefirmParser#fieldDef}.
	 * @param ctx the parse tree
	 */
	void exitFieldDef(BeefirmParser.FieldDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeefirmParser#typeDef}.
	 * @param ctx the parse tree
	 */
	void enterTypeDef(BeefirmParser.TypeDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeefirmParser#typeDef}.
	 * @param ctx the parse tree
	 */
	void exitTypeDef(BeefirmParser.TypeDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeefirmParser#commentDef}.
	 * @param ctx the parse tree
	 */
	void enterCommentDef(BeefirmParser.CommentDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeefirmParser#commentDef}.
	 * @param ctx the parse tree
	 */
	void exitCommentDef(BeefirmParser.CommentDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeefirmParser#methodDef}.
	 * @param ctx the parse tree
	 */
	void enterMethodDef(BeefirmParser.MethodDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeefirmParser#methodDef}.
	 * @param ctx the parse tree
	 */
	void exitMethodDef(BeefirmParser.MethodDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeefirmParser#paramSeq}.
	 * @param ctx the parse tree
	 */
	void enterParamSeq(BeefirmParser.ParamSeqContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeefirmParser#paramSeq}.
	 * @param ctx the parse tree
	 */
	void exitParamSeq(BeefirmParser.ParamSeqContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeefirmParser#paramDef}.
	 * @param ctx the parse tree
	 */
	void enterParamDef(BeefirmParser.ParamDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeefirmParser#paramDef}.
	 * @param ctx the parse tree
	 */
	void exitParamDef(BeefirmParser.ParamDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeefirmParser#horizontal_line}.
	 * @param ctx the parse tree
	 */
	void enterHorizontal_line(BeefirmParser.Horizontal_lineContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeefirmParser#horizontal_line}.
	 * @param ctx the parse tree
	 */
	void exitHorizontal_line(BeefirmParser.Horizontal_lineContext ctx);
}