// Generated from /Users/lafeier/dev/code/beefirm/src/main/java/Beefirm.g4 by ANTLR 4.9.2
package com.lafeier.beefirm.antlr4;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BeefirmParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, VISIBILITY=12, KW_CLASS=13, KW_INTERFACE=14, KW_ABSTRACT=15, 
		KW_SEMICOLON=16, KW_OVERRIDE=17, IDENTIFIER=18, SIMPLE_COMMENT=19, HORIZONTAL_LINE1=20, 
		HORIZONTAL_LINE2=21, HORIZONTAL_LINE3=22, HORIZONTAL_LINE4=23, BRACKETED_COMMENT=24, 
		WS=25;
	public static final int
		RULE_javaCode = 0, RULE_classDef = 1, RULE_extendDef = 2, RULE_implementDef = 3, 
		RULE_classBody = 4, RULE_fieldDef = 5, RULE_typeDef = 6, RULE_commentDef = 7, 
		RULE_methodDef = 8, RULE_paramSeq = 9, RULE_paramDef = 10, RULE_horizontal_line = 11;
	private static String[] makeRuleNames() {
		return new String[] {
			"javaCode", "classDef", "extendDef", "implementDef", "classBody", "fieldDef", 
			"typeDef", "commentDef", "methodDef", "paramSeq", "paramDef", "horizontal_line"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'extends'", "'implements'", "','", "'{'", "'}'", "'<'", "'['", 
			"'>'", "']'", "'('", "')'", null, "'class'", "'interface'", "'abstract'", 
			"';'", "'override'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			"VISIBILITY", "KW_CLASS", "KW_INTERFACE", "KW_ABSTRACT", "KW_SEMICOLON", 
			"KW_OVERRIDE", "IDENTIFIER", "SIMPLE_COMMENT", "HORIZONTAL_LINE1", "HORIZONTAL_LINE2", 
			"HORIZONTAL_LINE3", "HORIZONTAL_LINE4", "BRACKETED_COMMENT", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Beefirm.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BeefirmParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class JavaCodeContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(BeefirmParser.EOF, 0); }
		public List<ClassDefContext> classDef() {
			return getRuleContexts(ClassDefContext.class);
		}
		public ClassDefContext classDef(int i) {
			return getRuleContext(ClassDefContext.class,i);
		}
		public JavaCodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_javaCode; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).enterJavaCode(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).exitJavaCode(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BeefirmVisitor ) return ((BeefirmVisitor<? extends T>)visitor).visitJavaCode(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JavaCodeContext javaCode() throws RecognitionException {
		JavaCodeContext _localctx = new JavaCodeContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_javaCode);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(27);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << KW_CLASS) | (1L << KW_INTERFACE) | (1L << KW_ABSTRACT) | (1L << SIMPLE_COMMENT) | (1L << BRACKETED_COMMENT))) != 0)) {
				{
				{
				setState(24);
				classDef();
				}
				}
				setState(29);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(30);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassDefContext extends ParserRuleContext {
		public CommentDefContext commentDef;
		public List<CommentDefContext> comments = new ArrayList<CommentDefContext>();
		public Token kind;
		public Token name;
		public TerminalNode IDENTIFIER() { return getToken(BeefirmParser.IDENTIFIER, 0); }
		public TerminalNode KW_CLASS() { return getToken(BeefirmParser.KW_CLASS, 0); }
		public TerminalNode KW_INTERFACE() { return getToken(BeefirmParser.KW_INTERFACE, 0); }
		public TerminalNode KW_ABSTRACT() { return getToken(BeefirmParser.KW_ABSTRACT, 0); }
		public ExtendDefContext extendDef() {
			return getRuleContext(ExtendDefContext.class,0);
		}
		public ImplementDefContext implementDef() {
			return getRuleContext(ImplementDefContext.class,0);
		}
		public ClassBodyContext classBody() {
			return getRuleContext(ClassBodyContext.class,0);
		}
		public List<CommentDefContext> commentDef() {
			return getRuleContexts(CommentDefContext.class);
		}
		public CommentDefContext commentDef(int i) {
			return getRuleContext(CommentDefContext.class,i);
		}
		public ClassDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).enterClassDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).exitClassDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BeefirmVisitor ) return ((BeefirmVisitor<? extends T>)visitor).visitClassDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassDefContext classDef() throws RecognitionException {
		ClassDefContext _localctx = new ClassDefContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_classDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(35);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SIMPLE_COMMENT || _la==BRACKETED_COMMENT) {
				{
				{
				setState(32);
				((ClassDefContext)_localctx).commentDef = commentDef();
				((ClassDefContext)_localctx).comments.add(((ClassDefContext)_localctx).commentDef);
				}
				}
				setState(37);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(39);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KW_ABSTRACT) {
				{
				setState(38);
				match(KW_ABSTRACT);
				}
			}

			setState(41);
			((ClassDefContext)_localctx).kind = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==KW_CLASS || _la==KW_INTERFACE) ) {
				((ClassDefContext)_localctx).kind = (Token)_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(42);
			((ClassDefContext)_localctx).name = match(IDENTIFIER);
			setState(44);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__0) {
				{
				setState(43);
				extendDef();
				}
			}

			setState(47);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(46);
				implementDef();
				}
			}

			setState(50);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__3) {
				{
				setState(49);
				classBody();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExtendDefContext extends ParserRuleContext {
		public Token superClass;
		public TerminalNode IDENTIFIER() { return getToken(BeefirmParser.IDENTIFIER, 0); }
		public ExtendDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_extendDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).enterExtendDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).exitExtendDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BeefirmVisitor ) return ((BeefirmVisitor<? extends T>)visitor).visitExtendDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExtendDefContext extendDef() throws RecognitionException {
		ExtendDefContext _localctx = new ExtendDefContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_extendDef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(52);
			match(T__0);
			setState(53);
			((ExtendDefContext)_localctx).superClass = match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImplementDefContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public List<Token> superInterfaces = new ArrayList<Token>();
		public List<TerminalNode> IDENTIFIER() { return getTokens(BeefirmParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(BeefirmParser.IDENTIFIER, i);
		}
		public ImplementDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_implementDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).enterImplementDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).exitImplementDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BeefirmVisitor ) return ((BeefirmVisitor<? extends T>)visitor).visitImplementDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ImplementDefContext implementDef() throws RecognitionException {
		ImplementDefContext _localctx = new ImplementDefContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_implementDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(55);
			match(T__1);
			setState(56);
			((ImplementDefContext)_localctx).IDENTIFIER = match(IDENTIFIER);
			((ImplementDefContext)_localctx).superInterfaces.add(((ImplementDefContext)_localctx).IDENTIFIER);
			setState(61);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(57);
				match(T__2);
				setState(58);
				((ImplementDefContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				((ImplementDefContext)_localctx).superInterfaces.add(((ImplementDefContext)_localctx).IDENTIFIER);
				}
				}
				setState(63);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassBodyContext extends ParserRuleContext {
		public FieldDefContext fieldDef;
		public List<FieldDefContext> fields = new ArrayList<FieldDefContext>();
		public MethodDefContext methodDef;
		public List<MethodDefContext> methods = new ArrayList<MethodDefContext>();
		public List<FieldDefContext> fieldDef() {
			return getRuleContexts(FieldDefContext.class);
		}
		public FieldDefContext fieldDef(int i) {
			return getRuleContext(FieldDefContext.class,i);
		}
		public List<MethodDefContext> methodDef() {
			return getRuleContexts(MethodDefContext.class);
		}
		public MethodDefContext methodDef(int i) {
			return getRuleContext(MethodDefContext.class,i);
		}
		public ClassBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).enterClassBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).exitClassBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BeefirmVisitor ) return ((BeefirmVisitor<? extends T>)visitor).visitClassBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassBodyContext classBody() throws RecognitionException {
		ClassBodyContext _localctx = new ClassBodyContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_classBody);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(64);
			match(T__3);
			setState(69);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VISIBILITY) | (1L << KW_OVERRIDE) | (1L << IDENTIFIER) | (1L << SIMPLE_COMMENT) | (1L << HORIZONTAL_LINE1) | (1L << HORIZONTAL_LINE2) | (1L << HORIZONTAL_LINE3) | (1L << HORIZONTAL_LINE4) | (1L << BRACKETED_COMMENT))) != 0)) {
				{
				setState(67);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
				case 1:
					{
					setState(65);
					((ClassBodyContext)_localctx).fieldDef = fieldDef();
					((ClassBodyContext)_localctx).fields.add(((ClassBodyContext)_localctx).fieldDef);
					}
					break;
				case 2:
					{
					setState(66);
					((ClassBodyContext)_localctx).methodDef = methodDef();
					((ClassBodyContext)_localctx).methods.add(((ClassBodyContext)_localctx).methodDef);
					}
					break;
				}
				}
				setState(71);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(72);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldDefContext extends ParserRuleContext {
		public CommentDefContext commentDef;
		public List<CommentDefContext> comments = new ArrayList<CommentDefContext>();
		public TypeDefContext type;
		public Token fieldName;
		public TerminalNode KW_SEMICOLON() { return getToken(BeefirmParser.KW_SEMICOLON, 0); }
		public TerminalNode IDENTIFIER() { return getToken(BeefirmParser.IDENTIFIER, 0); }
		public Horizontal_lineContext horizontal_line() {
			return getRuleContext(Horizontal_lineContext.class,0);
		}
		public TerminalNode VISIBILITY() { return getToken(BeefirmParser.VISIBILITY, 0); }
		public TerminalNode KW_OVERRIDE() { return getToken(BeefirmParser.KW_OVERRIDE, 0); }
		public List<CommentDefContext> commentDef() {
			return getRuleContexts(CommentDefContext.class);
		}
		public CommentDefContext commentDef(int i) {
			return getRuleContext(CommentDefContext.class,i);
		}
		public TypeDefContext typeDef() {
			return getRuleContext(TypeDefContext.class,0);
		}
		public FieldDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).enterFieldDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).exitFieldDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BeefirmVisitor ) return ((BeefirmVisitor<? extends T>)visitor).visitFieldDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FieldDefContext fieldDef() throws RecognitionException {
		FieldDefContext _localctx = new FieldDefContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_fieldDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(75);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << HORIZONTAL_LINE1) | (1L << HORIZONTAL_LINE2) | (1L << HORIZONTAL_LINE3) | (1L << HORIZONTAL_LINE4))) != 0)) {
				{
				setState(74);
				horizontal_line();
				}
			}

			setState(80);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SIMPLE_COMMENT || _la==BRACKETED_COMMENT) {
				{
				{
				setState(77);
				((FieldDefContext)_localctx).commentDef = commentDef();
				((FieldDefContext)_localctx).comments.add(((FieldDefContext)_localctx).commentDef);
				}
				}
				setState(82);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(84);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==VISIBILITY) {
				{
				setState(83);
				match(VISIBILITY);
				}
			}

			setState(87);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KW_OVERRIDE) {
				{
				setState(86);
				match(KW_OVERRIDE);
				}
			}

			setState(90);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				{
				setState(89);
				((FieldDefContext)_localctx).type = typeDef();
				}
				break;
			}
			setState(92);
			((FieldDefContext)_localctx).fieldName = match(IDENTIFIER);
			setState(93);
			match(KW_SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeDefContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(BeefirmParser.IDENTIFIER, 0); }
		public List<TypeDefContext> typeDef() {
			return getRuleContexts(TypeDefContext.class);
		}
		public TypeDefContext typeDef(int i) {
			return getRuleContext(TypeDefContext.class,i);
		}
		public TypeDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).enterTypeDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).exitTypeDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BeefirmVisitor ) return ((BeefirmVisitor<? extends T>)visitor).visitTypeDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeDefContext typeDef() throws RecognitionException {
		TypeDefContext _localctx = new TypeDefContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_typeDef);
		int _la;
		try {
			setState(108);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(95);
				match(IDENTIFIER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(96);
				match(IDENTIFIER);
				setState(97);
				_la = _input.LA(1);
				if ( !(_la==T__5 || _la==T__6) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(98);
				typeDef();
				setState(103);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(99);
					match(T__2);
					setState(100);
					typeDef();
					}
					}
					setState(105);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(106);
				_la = _input.LA(1);
				if ( !(_la==T__7 || _la==T__8) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommentDefContext extends ParserRuleContext {
		public TerminalNode SIMPLE_COMMENT() { return getToken(BeefirmParser.SIMPLE_COMMENT, 0); }
		public TerminalNode BRACKETED_COMMENT() { return getToken(BeefirmParser.BRACKETED_COMMENT, 0); }
		public CommentDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_commentDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).enterCommentDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).exitCommentDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BeefirmVisitor ) return ((BeefirmVisitor<? extends T>)visitor).visitCommentDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommentDefContext commentDef() throws RecognitionException {
		CommentDefContext _localctx = new CommentDefContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_commentDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(110);
			_la = _input.LA(1);
			if ( !(_la==SIMPLE_COMMENT || _la==BRACKETED_COMMENT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodDefContext extends ParserRuleContext {
		public CommentDefContext commentDef;
		public List<CommentDefContext> comments = new ArrayList<CommentDefContext>();
		public TypeDefContext rtnType;
		public Token methodName;
		public ParamSeqContext paramSeq() {
			return getRuleContext(ParamSeqContext.class,0);
		}
		public TerminalNode KW_SEMICOLON() { return getToken(BeefirmParser.KW_SEMICOLON, 0); }
		public TerminalNode IDENTIFIER() { return getToken(BeefirmParser.IDENTIFIER, 0); }
		public Horizontal_lineContext horizontal_line() {
			return getRuleContext(Horizontal_lineContext.class,0);
		}
		public TerminalNode VISIBILITY() { return getToken(BeefirmParser.VISIBILITY, 0); }
		public TerminalNode KW_OVERRIDE() { return getToken(BeefirmParser.KW_OVERRIDE, 0); }
		public List<CommentDefContext> commentDef() {
			return getRuleContexts(CommentDefContext.class);
		}
		public CommentDefContext commentDef(int i) {
			return getRuleContext(CommentDefContext.class,i);
		}
		public TypeDefContext typeDef() {
			return getRuleContext(TypeDefContext.class,0);
		}
		public MethodDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).enterMethodDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).exitMethodDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BeefirmVisitor ) return ((BeefirmVisitor<? extends T>)visitor).visitMethodDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodDefContext methodDef() throws RecognitionException {
		MethodDefContext _localctx = new MethodDefContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_methodDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << HORIZONTAL_LINE1) | (1L << HORIZONTAL_LINE2) | (1L << HORIZONTAL_LINE3) | (1L << HORIZONTAL_LINE4))) != 0)) {
				{
				setState(112);
				horizontal_line();
				}
			}

			setState(118);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SIMPLE_COMMENT || _la==BRACKETED_COMMENT) {
				{
				{
				setState(115);
				((MethodDefContext)_localctx).commentDef = commentDef();
				((MethodDefContext)_localctx).comments.add(((MethodDefContext)_localctx).commentDef);
				}
				}
				setState(120);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(122);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==VISIBILITY) {
				{
				setState(121);
				match(VISIBILITY);
				}
			}

			setState(125);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KW_OVERRIDE) {
				{
				setState(124);
				match(KW_OVERRIDE);
				}
			}

			setState(128);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				{
				setState(127);
				((MethodDefContext)_localctx).rtnType = typeDef();
				}
				break;
			}
			setState(130);
			((MethodDefContext)_localctx).methodName = match(IDENTIFIER);
			setState(131);
			match(T__9);
			setState(132);
			paramSeq();
			setState(133);
			match(T__10);
			setState(134);
			match(KW_SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamSeqContext extends ParserRuleContext {
		public ParamDefContext paramDef;
		public List<ParamDefContext> params = new ArrayList<ParamDefContext>();
		public List<ParamDefContext> paramDef() {
			return getRuleContexts(ParamDefContext.class);
		}
		public ParamDefContext paramDef(int i) {
			return getRuleContext(ParamDefContext.class,i);
		}
		public ParamSeqContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paramSeq; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).enterParamSeq(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).exitParamSeq(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BeefirmVisitor ) return ((BeefirmVisitor<? extends T>)visitor).visitParamSeq(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamSeqContext paramSeq() throws RecognitionException {
		ParamSeqContext _localctx = new ParamSeqContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_paramSeq);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER || _la==BRACKETED_COMMENT) {
				{
				setState(136);
				((ParamSeqContext)_localctx).paramDef = paramDef();
				((ParamSeqContext)_localctx).params.add(((ParamSeqContext)_localctx).paramDef);
				}
			}

			setState(143);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(139);
				match(T__2);
				setState(140);
				((ParamSeqContext)_localctx).paramDef = paramDef();
				((ParamSeqContext)_localctx).params.add(((ParamSeqContext)_localctx).paramDef);
				}
				}
				setState(145);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamDefContext extends ParserRuleContext {
		public Token BRACKETED_COMMENT;
		public List<Token> comments = new ArrayList<Token>();
		public TypeDefContext type;
		public Token paramName;
		public TerminalNode IDENTIFIER() { return getToken(BeefirmParser.IDENTIFIER, 0); }
		public List<TerminalNode> BRACKETED_COMMENT() { return getTokens(BeefirmParser.BRACKETED_COMMENT); }
		public TerminalNode BRACKETED_COMMENT(int i) {
			return getToken(BeefirmParser.BRACKETED_COMMENT, i);
		}
		public TypeDefContext typeDef() {
			return getRuleContext(TypeDefContext.class,0);
		}
		public ParamDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paramDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).enterParamDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).exitParamDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BeefirmVisitor ) return ((BeefirmVisitor<? extends T>)visitor).visitParamDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamDefContext paramDef() throws RecognitionException {
		ParamDefContext _localctx = new ParamDefContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_paramDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(149);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BRACKETED_COMMENT) {
				{
				{
				setState(146);
				((ParamDefContext)_localctx).BRACKETED_COMMENT = match(BRACKETED_COMMENT);
				((ParamDefContext)_localctx).comments.add(((ParamDefContext)_localctx).BRACKETED_COMMENT);
				}
				}
				setState(151);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(153);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				{
				setState(152);
				((ParamDefContext)_localctx).type = typeDef();
				}
				break;
			}
			setState(155);
			((ParamDefContext)_localctx).paramName = match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Horizontal_lineContext extends ParserRuleContext {
		public TerminalNode HORIZONTAL_LINE1() { return getToken(BeefirmParser.HORIZONTAL_LINE1, 0); }
		public TerminalNode HORIZONTAL_LINE2() { return getToken(BeefirmParser.HORIZONTAL_LINE2, 0); }
		public TerminalNode HORIZONTAL_LINE3() { return getToken(BeefirmParser.HORIZONTAL_LINE3, 0); }
		public TerminalNode HORIZONTAL_LINE4() { return getToken(BeefirmParser.HORIZONTAL_LINE4, 0); }
		public Horizontal_lineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_horizontal_line; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).enterHorizontal_line(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BeefirmListener ) ((BeefirmListener)listener).exitHorizontal_line(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BeefirmVisitor ) return ((BeefirmVisitor<? extends T>)visitor).visitHorizontal_line(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Horizontal_lineContext horizontal_line() throws RecognitionException {
		Horizontal_lineContext _localctx = new Horizontal_lineContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_horizontal_line);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << HORIZONTAL_LINE1) | (1L << HORIZONTAL_LINE2) | (1L << HORIZONTAL_LINE3) | (1L << HORIZONTAL_LINE4))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\33\u00a2\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\3\2\7\2\34\n\2\f\2\16\2\37\13\2\3\2\3\2\3\3\7\3"+
		"$\n\3\f\3\16\3\'\13\3\3\3\5\3*\n\3\3\3\3\3\3\3\5\3/\n\3\3\3\5\3\62\n\3"+
		"\3\3\5\3\65\n\3\3\4\3\4\3\4\3\5\3\5\3\5\3\5\7\5>\n\5\f\5\16\5A\13\5\3"+
		"\6\3\6\3\6\7\6F\n\6\f\6\16\6I\13\6\3\6\3\6\3\7\5\7N\n\7\3\7\7\7Q\n\7\f"+
		"\7\16\7T\13\7\3\7\5\7W\n\7\3\7\5\7Z\n\7\3\7\5\7]\n\7\3\7\3\7\3\7\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\7\bh\n\b\f\b\16\bk\13\b\3\b\3\b\5\bo\n\b\3\t\3\t\3"+
		"\n\5\nt\n\n\3\n\7\nw\n\n\f\n\16\nz\13\n\3\n\5\n}\n\n\3\n\5\n\u0080\n\n"+
		"\3\n\5\n\u0083\n\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\5\13\u008c\n\13\3\13\3"+
		"\13\7\13\u0090\n\13\f\13\16\13\u0093\13\13\3\f\7\f\u0096\n\f\f\f\16\f"+
		"\u0099\13\f\3\f\5\f\u009c\n\f\3\f\3\f\3\r\3\r\3\r\2\2\16\2\4\6\b\n\f\16"+
		"\20\22\24\26\30\2\7\3\2\17\20\3\2\b\t\3\2\n\13\4\2\25\25\32\32\3\2\26"+
		"\31\2\u00ae\2\35\3\2\2\2\4%\3\2\2\2\6\66\3\2\2\2\b9\3\2\2\2\nB\3\2\2\2"+
		"\fM\3\2\2\2\16n\3\2\2\2\20p\3\2\2\2\22s\3\2\2\2\24\u008b\3\2\2\2\26\u0097"+
		"\3\2\2\2\30\u009f\3\2\2\2\32\34\5\4\3\2\33\32\3\2\2\2\34\37\3\2\2\2\35"+
		"\33\3\2\2\2\35\36\3\2\2\2\36 \3\2\2\2\37\35\3\2\2\2 !\7\2\2\3!\3\3\2\2"+
		"\2\"$\5\20\t\2#\"\3\2\2\2$\'\3\2\2\2%#\3\2\2\2%&\3\2\2\2&)\3\2\2\2\'%"+
		"\3\2\2\2(*\7\21\2\2)(\3\2\2\2)*\3\2\2\2*+\3\2\2\2+,\t\2\2\2,.\7\24\2\2"+
		"-/\5\6\4\2.-\3\2\2\2./\3\2\2\2/\61\3\2\2\2\60\62\5\b\5\2\61\60\3\2\2\2"+
		"\61\62\3\2\2\2\62\64\3\2\2\2\63\65\5\n\6\2\64\63\3\2\2\2\64\65\3\2\2\2"+
		"\65\5\3\2\2\2\66\67\7\3\2\2\678\7\24\2\28\7\3\2\2\29:\7\4\2\2:?\7\24\2"+
		"\2;<\7\5\2\2<>\7\24\2\2=;\3\2\2\2>A\3\2\2\2?=\3\2\2\2?@\3\2\2\2@\t\3\2"+
		"\2\2A?\3\2\2\2BG\7\6\2\2CF\5\f\7\2DF\5\22\n\2EC\3\2\2\2ED\3\2\2\2FI\3"+
		"\2\2\2GE\3\2\2\2GH\3\2\2\2HJ\3\2\2\2IG\3\2\2\2JK\7\7\2\2K\13\3\2\2\2L"+
		"N\5\30\r\2ML\3\2\2\2MN\3\2\2\2NR\3\2\2\2OQ\5\20\t\2PO\3\2\2\2QT\3\2\2"+
		"\2RP\3\2\2\2RS\3\2\2\2SV\3\2\2\2TR\3\2\2\2UW\7\16\2\2VU\3\2\2\2VW\3\2"+
		"\2\2WY\3\2\2\2XZ\7\23\2\2YX\3\2\2\2YZ\3\2\2\2Z\\\3\2\2\2[]\5\16\b\2\\"+
		"[\3\2\2\2\\]\3\2\2\2]^\3\2\2\2^_\7\24\2\2_`\7\22\2\2`\r\3\2\2\2ao\7\24"+
		"\2\2bc\7\24\2\2cd\t\3\2\2di\5\16\b\2ef\7\5\2\2fh\5\16\b\2ge\3\2\2\2hk"+
		"\3\2\2\2ig\3\2\2\2ij\3\2\2\2jl\3\2\2\2ki\3\2\2\2lm\t\4\2\2mo\3\2\2\2n"+
		"a\3\2\2\2nb\3\2\2\2o\17\3\2\2\2pq\t\5\2\2q\21\3\2\2\2rt\5\30\r\2sr\3\2"+
		"\2\2st\3\2\2\2tx\3\2\2\2uw\5\20\t\2vu\3\2\2\2wz\3\2\2\2xv\3\2\2\2xy\3"+
		"\2\2\2y|\3\2\2\2zx\3\2\2\2{}\7\16\2\2|{\3\2\2\2|}\3\2\2\2}\177\3\2\2\2"+
		"~\u0080\7\23\2\2\177~\3\2\2\2\177\u0080\3\2\2\2\u0080\u0082\3\2\2\2\u0081"+
		"\u0083\5\16\b\2\u0082\u0081\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0084\3"+
		"\2\2\2\u0084\u0085\7\24\2\2\u0085\u0086\7\f\2\2\u0086\u0087\5\24\13\2"+
		"\u0087\u0088\7\r\2\2\u0088\u0089\7\22\2\2\u0089\23\3\2\2\2\u008a\u008c"+
		"\5\26\f\2\u008b\u008a\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u0091\3\2\2\2"+
		"\u008d\u008e\7\5\2\2\u008e\u0090\5\26\f\2\u008f\u008d\3\2\2\2\u0090\u0093"+
		"\3\2\2\2\u0091\u008f\3\2\2\2\u0091\u0092\3\2\2\2\u0092\25\3\2\2\2\u0093"+
		"\u0091\3\2\2\2\u0094\u0096\7\32\2\2\u0095\u0094\3\2\2\2\u0096\u0099\3"+
		"\2\2\2\u0097\u0095\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u009b\3\2\2\2\u0099"+
		"\u0097\3\2\2\2\u009a\u009c\5\16\b\2\u009b\u009a\3\2\2\2\u009b\u009c\3"+
		"\2\2\2\u009c\u009d\3\2\2\2\u009d\u009e\7\24\2\2\u009e\27\3\2\2\2\u009f"+
		"\u00a0\t\6\2\2\u00a0\31\3\2\2\2\33\35%).\61\64?EGMRVY\\insx|\177\u0082"+
		"\u008b\u0091\u0097\u009b";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}