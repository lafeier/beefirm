// Generated from /Users/lafeier/dev/code/beefirm/src/main/java/Beefirm.g4 by ANTLR 4.9.2
package com.lafeier.beefirm.antlr4;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link BeefirmParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface BeefirmVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link BeefirmParser#javaCode}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJavaCode(BeefirmParser.JavaCodeContext ctx);
	/**
	 * Visit a parse tree produced by {@link BeefirmParser#classDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDef(BeefirmParser.ClassDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link BeefirmParser#extendDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtendDef(BeefirmParser.ExtendDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link BeefirmParser#implementDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImplementDef(BeefirmParser.ImplementDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link BeefirmParser#classBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassBody(BeefirmParser.ClassBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link BeefirmParser#fieldDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFieldDef(BeefirmParser.FieldDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link BeefirmParser#typeDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeDef(BeefirmParser.TypeDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link BeefirmParser#commentDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCommentDef(BeefirmParser.CommentDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link BeefirmParser#methodDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodDef(BeefirmParser.MethodDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link BeefirmParser#paramSeq}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamSeq(BeefirmParser.ParamSeqContext ctx);
	/**
	 * Visit a parse tree produced by {@link BeefirmParser#paramDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamDef(BeefirmParser.ParamDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link BeefirmParser#horizontal_line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHorizontal_line(BeefirmParser.Horizontal_lineContext ctx);
}