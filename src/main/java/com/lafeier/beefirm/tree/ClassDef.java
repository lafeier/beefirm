package com.lafeier.beefirm.tree;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class ClassDef {
    boolean abstracted = false;
    String kind;
    String className;
    String superClassName;
    List<String> superInterfaceList = new ArrayList<>();
    List<String> comments = new ArrayList<>();
    List<FieldDef> fieldList = new ArrayList<>();
    List<MethodDef> methodList = new ArrayList<>();
}
