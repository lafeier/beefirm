package com.lafeier.beefirm.tree;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class FieldDef {
    String visibility;
    String type;
    String fieldName;
    List<String> comments;
    String horizontalLine;
    boolean override;
}
