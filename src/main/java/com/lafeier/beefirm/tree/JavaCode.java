package com.lafeier.beefirm.tree;

import java.util.ArrayList;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JavaCode {
    List<ClassDef> classList = new ArrayList<>();
    public boolean isDefine(String className){
        return classList.stream().anyMatch(classDef -> className.equals(classDef.getClassName()));
    }
}
