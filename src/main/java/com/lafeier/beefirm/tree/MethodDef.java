package com.lafeier.beefirm.tree;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@Builder
public class MethodDef {

    @Data
    @Builder
    public static class MethodParam {
        String paramType;
        String paramName;
        List<String> comments;
    }

    String visibility;
    String returnType;
    String methodName;
    List<MethodParam> paramList;
    List<String> comments;
    String horizontalLine;
    boolean override;

    public String getParamListString(boolean hasBracket){
        String paramListNoBracket = getParamList().stream().map(methodParam -> String.join(" ",
                Optional.ofNullable(methodParam.getParamType()).orElse(""),
                methodParam.getParamName())).collect(Collectors.joining(","));
        if (hasBracket){
            return String.format("( %s )",paramListNoBracket);
        }
        return paramListNoBracket;
    }

}
